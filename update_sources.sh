# Remove what was previously here
rm -rf sources
rm repos_list

process_slug()
{
	SLUG="$1"
	DIR="sources/$1"

	echo "Process $1"
	mkdir -p "$DIR"

	# Get the list of repositories to get
	wget -O - https://api.github.com/orgs/$SLUG/repos | grep html_url | cut -d'"' -f 4 | grep -v "^https://github.com/$SLUG$" >> repos_list


	for repo in $(cat repos_list);
	do
		URL="$repo"
		DEST="sources/$(echo "$repo" | sed 's/https:\/\/github.com\///g' -E)"
		echo "Download $URL into $DEST"
		git clone "$URL" "$DEST"
		
		echo "Remove GIT repository of project"
		rm -rf "$DEST/.git"
	done

}

process_slug eu-digital-green-certificates
process_slug ehn-dcc-development